package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC003_DeleteLead extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC003_DeleteLead";
		testCaseDescription ="Delete lead";
		category = "Smoke";
		author= "sainu";
		dataSheetName="TC003";
	}
	@Test(dataProvider="fetchData")
	public  void createLead(String email, String errorMsg) throws InterruptedException   {
		new MyHomePage()
		.clickLeads()
		.clickFindLeads()
		.ClickEmailTab()
		.typeEmailInfo(email)
		.clickFindLeadsSearch()
		.GetFirstFindLeadLinkLeadId()
		.clickFirstFindLeadLink()
		.clickDeleteLead()
		.clickFindLeads()
		.typeLeadID()
		.clickFindLeadsSearch()
		.VerifyDeletedLeadInfo(errorMsg);
		
		
		
	}
	

}
