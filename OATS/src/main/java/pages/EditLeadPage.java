package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{

	public EditLeadPage typeEditCompanyName(String data) {
		WebElement eleEditCompanyName = locateElement("id", "updateLeadForm_companyName");
		type(eleEditCompanyName, data);
		return this;
	}
	
	
	
	public ViewLeadPage clickEditLeadUpdate() {
		WebElement eleEditLeadUpdate= locateElement("name", "submitButton");
		click(eleEditLeadUpdate);
		return new ViewLeadPage(); 
	}
	
}









