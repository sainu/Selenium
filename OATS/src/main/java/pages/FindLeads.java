package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class FindLeads extends ProjectMethods{
	String LeadId,LeadName;
	public FindLeads ClickPhoneTab() {
		WebElement elePhoneTab = locateElement("xpath", "//span[text()='Phone']");
		click(elePhoneTab);
		return this;
	}
	
	public FindLeads typePhoneNumber(String data) {
		WebElement elePhoneNumber = locateElement("name", "phoneNumber");
		type(elePhoneNumber, data);
		return this;
	}
	
	public FindLeads ClickEmailTab() {
		WebElement eleEmailTab = locateElement("xpath", "//span[text()='Email']");
		click(eleEmailTab);
		return this;
	}
	
	public FindLeads typeEmailInfo(String data) {
		WebElement eleEmailInfo = locateElement("name", "emailAddress");
		type(eleEmailInfo, data);
		return this;
	}
	
	public FindLeads typeLeadID() {
		WebElement eleLeadID = locateElement("name", "id");
		type(eleLeadID, LeadId);
		return this;
	}
	
	public FindLeads typeFindLeadFirstName(String fname) {
		WebElement eleFindLeadFirstName = locateElement("name", "firstName");
		type(eleFindLeadFirstName, fname);
		return this;
	}
	
	public FindLeads MergeLeadSwitchWindowOne() {
		switchToWindow(1);
		return this;
	}	
	public FindLeads clickFindLeadsSearch() {
		WebElement eleFindLeadsSearch = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeadsSearch);
		return this;
	}
	
	
	public FindLeads GetFirstFindLeadLinkLeadId() throws InterruptedException {
		Thread.sleep(2000);
		LeadId= getText(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		return this;
	}

	public FindLeads GetFirstFindLeadLinkLeadName() throws InterruptedException {
		Thread.sleep(2000);
		LeadName= getText(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a[1]"));
		return this;
	}
	
	public FindLeads VerifyDeletedLeadInfo(String errorMsg) throws InterruptedException {
		Thread.sleep(2000);
		WebElement eleVerify = locateElement("class", "x-paging-info");
		verifyExactText(eleVerify, errorMsg);
		return this;
	}
	public FindLeads VerifyMergedLeadInfo(String errorMsg) throws InterruptedException {
		Thread.sleep(2000);
		WebElement eleMergedLeadInfo = locateElement("class", "x-paging-info");
		verifyExactText(eleMergedLeadInfo, errorMsg);
		return this;
	}
	
	
	public ViewLeadPage clickFirstFindLeadLink() throws InterruptedException {
		Thread.sleep(2000);
		WebElement eleFirstFindLeadLink = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleFirstFindLeadLink);
		return new ViewLeadPage();
	}
	
	public MergeLead clickMergeFirstFindLeadLink() throws InterruptedException {
		Thread.sleep(2000);
		WebElement eleMergeFirstFindLeadLink = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleMergeFirstFindLeadLink);
		return new MergeLead();
	}
	
	
}









